# Copyright (c) 2016-2018, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project, see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

import os
import os.path

import django.db.models
import django.dispatch
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

import th_bootstrap.forms.fields
import th_bootstrap.views.upload


class AjaxBootstrapFileInputFilename(object):
    def __init__(self, file_storage, f_name):
        self.full_file_path = os.path.join(settings.PERSISTENT_DIR, file_storage, f_name)
        self.file_name = f_name

    def __str__(self):
        return self.file_name


class AjaxBootstrapFileInput(django.db.models.Field):
    description = _("Field for a path to an uploaded file")
    form_class = th_bootstrap.forms.fields.AjaxBootstrapFileInput

    def __init__(self, verbose_name=None, name=None, file_storage=None, allow_download=False, auto_upload=False,
                 show_preview=False,
                 file_extensions=None,
                 upload_view=th_bootstrap.views.upload.UploadFile,
                 postupload_view=th_bootstrap.views.upload.PostUploadFile,
                 download_view=th_bootstrap.views.upload.DownloadFile,
                 single_file_check_function=None,
                 multi_file_check_function=None,
                 show_messages=False,
                 messages = None,
                 allow_multiple_files=False,
                 show_current_file_name=False,
                 **kwargs):
        self.upload_view = upload_view
        self.post_upload_view = postupload_view
        self.download_view = download_view
        self.file_storage = file_storage
        self.allow_download = allow_download
        self.show_current_file_name = show_current_file_name
        self.auto_upload = auto_upload
        self.show_preview = show_preview
        self.single_file_check_function = single_file_check_function
        self.multi_file_check_function = multi_file_check_function
        self.show_messages = show_messages
        self.messages = messages
        self.allow_multiple_files = allow_multiple_files
        self.multi_check_result = None

        if isinstance(file_extensions, str):
            file_extensions = [file_extensions]

        self.file_extensions = file_extensions
        self._filename = None
        super().__init__(verbose_name, name, **kwargs)

    @property
    def filename(self):
        return self._filename

    @property
    def full_file_path(self):
        return os.path.join(settings.PERSISTENT_DIR, self.file_storage, self._filename)


    def db_type(self, connection):
        return "TEXT"

    def to_python(self, value):
        return value

    def from_db_value(self, value, expression, connection):
        return AjaxBootstrapFileInputFilename(self.file_storage, value)

    def get_prep_value(self, value):
        if isinstance(value, str):
            return value
        elif isinstance(value, AjaxBootstrapFileInputFilename):
            return value.file_name

    def validate(self, value, model_instance):
        super().validate(value, model_instance)

    def formfield(self, **kwargs):
        defaults = {'form_class': self.form_class, 'modelfield_instance': self}
        defaults.update(kwargs)
        return super().formfield(**defaults)

    def deconstruct(self):
        name, path, args, kwargs = super().deconstruct()

        return name, path, args, kwargs

    def __str__(self):
        return self._filename

    @staticmethod
    @django.dispatch.receiver(django.db.models.signals.pre_delete)
    def on_delete(sender, **kwargs):
        bootstrap_fields = [this_field for this_field in kwargs['instance'].__dict__.values() if
                            isinstance(this_field, AjaxBootstrapFileInput)]

        for field in bootstrap_fields:
            if field._filename:
                try:
                    os.remove(os.path.join(field.file_storage, field._filename))
                except FileNotFoundError:
                    pass
