# Copyright (c) 2016-2018, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project, see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

import django.template.context
import django_tables2.views
from django_tables2 import Table
from django_tables2.tables import TableOptions


class BootstrapTableOptions(TableOptions):
    def __init__(self, options=None):
        super().__init__(options)

        default_data = {
            'pagination': False,
            'toggle': 'data',
            'classes': None,
            'striped': False,
            'search': False
        }

        self.id = getattr(options, 'id', 'table')
        self.html_class = getattr(options, 'html_class', 'table')
        self.data = default_data
        self.data.update(getattr(options, 'data', default_data))


class BootstrapTable(Table):
    class Meta:
        orderable = False

    def __init__(self, data, **kwargs):
        super().__init__(data, **kwargs)
        self._meta = BootstrapTableOptions(getattr(self, 'Meta', None))
        self.attrs.update({
            'id': self._meta.id,
            'class': self._meta.html_class,
        })

        for data, value in self._meta.data.items():
            if isinstance(value, bool):
                val = str(value).lower()
            else:
                val = str(value)
            self.attrs.update({'data-%s' % (data,): val})

        for cur_column in self.columns:
            if 'th' not in cur_column.attrs:
                cur_column.attrs['th'] = {}
            if 'th' not in cur_column.column.attrs:
                cur_column.column.attrs['th'] = {}
            if 'data-field' not in cur_column.attrs['th']:
                cur_column.column.attrs['th']['data-field'] = cur_column.verbose_name
                pass

    def get_column_class_names(self, classes_set, bound_column):
        classes_set.add('table_%s' % (bound_column.name,))
        return classes_set

    def before_render(self, request):
        super().before_render(request)
        self.context['perms'] = django.template.context.make_context(self.context.flatten())['perms']
        pass


class GenericTable(BootstrapTable):
    class Meta(BootstrapTable.Meta):
        data = {
            'striped': True,
            'classes': 'table-no-bordered',
            'search': True,
            'pagination': True
        }


class SingleTableMixin(django_tables2.views.SingleTableMixin):
    table_pagination = False
