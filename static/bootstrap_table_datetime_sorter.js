/*
 * Copyright (c) 2016-2018, Thomas Hartmann
 *
 * This file is part of the OBOB Subject Database Project, see: https://gitlab.com/obob/obob_subjectdb/
 *
 *    obob_subjectdb is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    obob_subjectdb is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.
 */

define(['jquery', 'sprintf-js', 'deepcopy', 'bootstrap-table'], function ($, sprintf, deepcopy) {
    date_finder = function (table_data, field_name, date_string) {
        for (i = 0; i < table_data.length; i++) {
            if (table_data[i][field_name] == date_string) {
                return table_data[i][sprintf.sprintf('_%s_data', field_name)]['datetime-iso'];
            }
        }
    };

    date_sorter = function (table_data, field_id, a, b) {
        var date_a = Date.parse(date_finder(table_data, field_id, a));
        var date_b = Date.parse(date_finder(table_data, field_id, b));
        return date_a - date_b;
    };

    find_column = function(columns, field) {
        columns = columns[0];
        for(i=0; i<columns.length; i++) {
            if(columns[i].field == field) {
                return i;
            }
        }

        return -1;
    };


    return function (id_table) {
        var table = $('#' + id_table);
        var cols = table.bootstrapTable('getOptions').columns;

        table.find('th.data-has-iso-datetime').each(function (i) {
            var field = $(this).data('field');
            var field_id = find_column(cols, field);
            if (field_id >= 0) {
                var table_data = deepcopy(table.bootstrapTable('getOptions').data);

                cols[0][field_id].sorter = function (a, b) {
                    return date_sorter(table_data, field, a, b);
                };
            }
        });

        table.bootstrapTable('refreshOptions', {
            columns: cols
        });
    };
});