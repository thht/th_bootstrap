/*
 * Copyright (c) 2016-2018, Thomas Hartmann
*
* This file is part of the OBOB Subject Database Project, see: https://gitlab.com/obob/obob_subjectdb/
*
*    obob_subjectdb is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    obob_subjectdb is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.
*/

define(["jquery", "js-cookie", "bootstrap_alerts", "bootstrap-fileinput"], function($, Cookies, bs_alerts) {
    return function(input_element) {
        input_element = $(input_element);
        var hidden_files = $('#' + input_element.attr('name') + '_files');
        var hidden_uuid = $('#' + input_element.attr('name') + '_uuid');
        var hidden_salt = $('#' + input_element.attr('name') + '_salt');
        var hidden_multicheck_function = $('#' + input_element.attr('name') + '_multi_file_check_function');
        var csrftoken = Cookies.get('csrftoken');
        var error_stack = [];

        // get and parse extra input fields...
        var all_hidden_input = input_element.data('fileuploadHiddeninput');
        var auto_upload = input_element.data('fileuploadAutoupload') === 'True';
        var file_extension = input_element.data('fileuploadFileextensions');
        var allow_multiple_files = input_element.data('fileuploadAllowmultiplefiles') === 'True';
        var show_messages = input_element.data('fileuploadShowmessages') === 'True';
        var post_upload_url = input_element.data('fileuploadPostuploadurl');
        var is_required = input_element.data('fileuploadRequired');

        var messages_div = null;
        var messages = null;
        if (show_messages) {
            messages_div = $('#' + input_element.attr('name') + '_messages');
            messages = input_element.data('fileuploadMessages');
        }

        function display_message(msg_class, msg) {
            if (show_messages) {
                if (msg) {
                    bs_alerts(messages_div, msg_class, msg);
                }
            }
        }

        function set_valid(valid) {
            if (is_required) {
                input_element[0].required = !valid;
            }

            if (valid) {
                input_element[0].setCustomValidity('');
            }
            else {
                input_element[0].setCustomValidity('Please upload a valid File');
            }

            input_element.change();
        }

        function reset_fileinput() {
            input_element.fileinput('clear').fileinput('unlock');
            error_stack.length = 0;
            hidden_files.val('');
            if (show_messages) {
                bs_alerts(messages_div, 'alert-info', '');
            }
            set_valid(false);
        }

        $.each(all_hidden_input, function (key, value) {
            all_hidden_input[key] = $('#' + value).val();
        });

        var js_attrs = {
            'uploadExtraData': {
                'csrfmiddlewaretoken': csrftoken
            },
            'uploadAsync': true,
            'uploadUrl': input_element.data('fileuploadUploadurl'),
            'showUpload': !auto_upload,
            'showRemove': false,
            'showClose': false,
            'showPreview': input_element.data('fileuploadShowpreview') === 'True',
            'autoReplace': true,
            'showCaption': input_element.data('fileuploadShowpreview') !== 'True',
            'showCancel': false,
            'fileActionSettings': {
                'showRemove': false,
                'showZoom': false,
                'showUpload': false
            }
        };

        if (show_messages) {
            js_attrs['msgErrorClass'] = 'hidden';
        }

        if (file_extension) {
            js_attrs['allowedFileExtensions'] = file_extension;
        }

        if (!allow_multiple_files) {
            js_attrs['maxFileCount'] = 1;
            js_attrs['autoReplace'] = true;
        }

        $.extend(js_attrs['uploadExtraData'], all_hidden_input);

        js_attrs['slugCallback'] = function (filename) {
            return filename;
        };

        input_element.fileinput(js_attrs);

        input_element.on('fileuploaded', function (event, data) {
            hidden_files.val(function (index, value) {
                return value + data.response.files[0] + ';';
            });
        });

        input_element.on('filebrowse', function (event) {
            reset_fileinput();
        });

        input_element.on('filepreupload', function(event) {
            input_element.fileinput('lock').fileinput('disable');
            display_message('alert-info', messages['upload_started']);
        });

        input_element.on('filebatchuploadcomplete', function(event, files, extra) {
            input_element.fileinput('lock').fileinput('disable');
            if (post_upload_url.length > 0) {
                display_message('alert-info', messages['check_multi']);

                // prepare data....
                var data = {
                    files: hidden_files.val(),
                    uuid: hidden_uuid.val(),
                    salt: hidden_salt.val(),
                    multicheck_function: hidden_multicheck_function.val()
                };

                $.ajax({
                    url: post_upload_url,
                    method: 'POST',
                    data: data,
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('X-CSRFToken', csrftoken);
                    },
                    success: function (result) {
                        if (result['status'] === true) {
                            if (result['message']) {
                                display_message('alert-success', result['message']);
                                set_valid(true);
                            }
                            else {
                                display_message('alert-success', messages['post_upload']);
                                set_valid(true);
                            }
                        }
                        else {
                            display_message('alert-danger', result['message']);
                            set_valid(false);
                        }
                        input_element.fileinput('unlock').fileinput('enable');
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        display_message('alert-danger', 'Upload request failed with error: ' + textStatus + '; ' + errorThrown);
                        set_valid(false);
                        input_element.fileinput('unlock').fileinput('enable');
                    }
                })
            }
            else {
                if (hidden_files.val().length > 0) {
                    display_message('alert-success', messages['post_upload']);
                    set_valid(true);
                }
                else {
                    set_valid(false);
                }
                input_element.fileinput('unlock').fileinput('enable');
            }
        });

        input_element.on('fileuploaderror', function (event, data, msg) {
            console.log('Error');
            reset_fileinput();
            error_stack.push(msg);
            display_message('alert-danger', msg);
            set_valid(false);
            input_element.fileinput('unlock').fileinput('enable');
        });

        if (auto_upload) {
            input_element.on('filebatchselected', function (event) {
                if (error_stack.length === 0) {
                    input_element.fileinput('upload');
                }
            });
        }
    }
});