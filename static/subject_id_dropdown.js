/*
 * Copyright (c) 2016-2018, Thomas Hartmann
*
* This file is part of the OBOB Subject Database Project, see: https://gitlab.com/obob/obob_subjectdb/
*
*    obob_subjectdb is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    obob_subjectdb is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * Created by th on 04.05.16.
 */

define(['jquery', 'jquery-dropdown', 'subject_id_dropdown_helpers', 'sprintf-js'], function($, jq_dropdown, helpers, sprintf) {
    var dropdown_counter = 0;

    return function(target, submit_form) {
        var this_dropdown_counter = ++dropdown_counter;
        var this_id = 'jq-dropdown-' + this_dropdown_counter;
        var this_input = $(target);
        var content = $('<div class="jq-dropdown-panel jq-dropdown-panel-wide"></div>').load('/static/subject_id_dialog.html', function() {
            $('body').append(sprintf.sprintf('<div class="jq-dropdown-panel-wide jq-dropdown" id=%s></div>', this_id));
            $('#' + this_id).append(content[0]);

            this_input.jqDropdown('attach', '#' + this_id);

            helpers(this_id, this_input, submit_form);
        });



    }
});