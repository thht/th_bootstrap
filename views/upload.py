# Copyright (c) 2016-2018, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project, see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

import json
import os
import os.path
import pathlib

import django.core.signing
from django.conf import settings
from django.core.exceptions import ValidationError
from django.http import FileResponse, JsonResponse
from django.views.generic import View

import th_django.utils.fun2string
from th_django.views import urlmixin_kwargs
from th_django.views.mixins import AutoUrlMixin


class UploadFile(View, AutoUrlMixin):
    http_method_names = ['post']
    upload_tmp = settings.BOOTSTRAP_FILEINPUT['UPLOAD_TEMP']

    def post(self, request):
        upload_tmp = self.upload_tmp
        salt = request.POST['salt']
        signer = django.core.signing.Signer(salt=salt)
        file_extensions = json.loads(signer.unsign(request.POST['allowed_fileextensions']))
        uuid = signer.unsign(request.POST['uuid'])
        single_file_check_function = None
        try:
            single_file_check_function = th_django.utils.fun2string.string2fun(
                signer.unsign(request.POST['single_file_check_function']))
        except KeyError:
            pass

        if not os.path.isdir(upload_tmp):
            try:
                os.makedirs(upload_tmp)
            except FileExistsError:
                pass

        tmpdir = os.path.join(upload_tmp, uuid)
        if not os.path.isdir(tmpdir):
            try:
                os.mkdir(tmpdir)
            except FileExistsError:
                pass

        response = {'files': []}
        for file in request.FILES.values():
            if file_extensions and pathlib.Path(file.name).suffix[1:] not in file_extensions:
                raise ValidationError('Not allowed file extension')

            if single_file_check_function:
                rval = single_file_check_function(file)
                if rval:
                    response['error'] = rval
                    return JsonResponse(response)

            with open(os.path.join(tmpdir, file.name), 'wb') as fid:
                file.seek(0)
                fid.write(file.read())
            response['files'].append(file.name)

        return JsonResponse(response)


class PostUploadFile(View, AutoUrlMixin):
    http_method_names = ['post']
    upload_tmp = settings.BOOTSTRAP_FILEINPUT['UPLOAD_TEMP']

    def post(self, request):
        salt = request.POST['salt']
        signer = django.core.signing.Signer(salt=salt)
        uuid = signer.unsign(request.POST['uuid'])

        multi_check_function = None
        rval = {
            'status': True,
            'message': ''
        }

        if not request.POST['files']:
            rval['status'] = False
            rval['message'] = 'Did not get any files.'

            return rval

        all_files = request.POST['files'].split(';')
        all_files.pop()

        tmp_path = pathlib.Path(os.path.join(self.upload_tmp, uuid))
        for cur_file in tmp_path.iterdir():
            if cur_file.name not in all_files:
                cur_file.unlink()

        try:
            multi_check_function = th_django.utils.fun2string.string2fun(
                signer.unsign(request.POST['multicheck_function'])
            )
        except KeyError:
            pass

        if multi_check_function:
            rval = multi_check_function(uuid, self.upload_tmp, all_files)
            del rval['file_type']

        return JsonResponse(rval)


class DownloadFile(View, AutoUrlMixin):
    http_method_names = ['get', 'post']
    url_kwargs = [urlmixin_kwargs.string('filename'), urlmixin_kwargs.string('salt')]

    def dispatch(self, request, filename, salt):
        filename = django.core.signing.Signer(salt=salt).unsign(filename)

        response = FileResponse(open(os.path.join(settings.PERSISTENT_DIR, filename), 'rb'))
        response['Content-Disposition'] = 'attachment; filename=%s' % (os.path.basename(filename))

        return response
