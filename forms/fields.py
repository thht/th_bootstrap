# Copyright (c) 2016-2018, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project, see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

import os.path
import shutil
import zipfile

import django.conf
import django.core.exceptions
import django.forms
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

import th_bootstrap.widgets.ajaxbootstrap_fileinput


class AjaxBootstrapFileInput(django.forms.Field):
    default_error_messages = {
        'required': _('Please upload your File before submitting')
    }

    widget = th_bootstrap.widgets.ajaxbootstrap_fileinput.AjaxBootstrapFileInput

    def __init__(self, modelfield_instance, *args, **kwargs):
        self.modelfield_instance = modelfield_instance
        self.file_storage = self.modelfield_instance.file_storage
        super().__init__(*args, **kwargs)
        self.widget.modelfield_instance = modelfield_instance

    def to_python(self, value):
        if not value['data']:
            return self.initial.file_name
        self.uuid = value['uuid']
        files_string = value['data']

        tmpdir = self._get_tmpdir()
        tmpfiles = files_string.split(';')

        final_storage = os.path.join(settings.PERSISTENT_DIR, self.file_storage, self.uuid)

        if os.path.exists(os.path.join(final_storage, tmpfiles[0])):
            return os.path.join(self.uuid, tmpfiles[0])

        tmpfiles.pop()

        if (not self.modelfield_instance.allow_multiple_files) and len(tmpfiles) > 1:
            raise django.core.exceptions.ValidationError(_('Multiple files received in Single File Widget'),
                                                         code='multiplefiles')

        if not self.modelfield_instance.allow_multiple_files:
            src_file = os.path.join(tmpdir, tmpfiles[0])
            dst_file = os.path.join(final_storage, tmpfiles[0])
        else:
            src_file = tmpdir
            dst_file = '%s.zip' % (final_storage,)

        self.process_pre_check(value, self.uuid, tmpfiles)

        if self.modelfield_instance.single_file_check_function:
            with open(src_file, 'rb') as file_object:
                rval = self.modelfield_instance.single_file_check_function(file_object)
                if rval is not None:
                    raise django.core.exceptions.ValidationError(_('File validation failed'),
                                                                 code='filevalidationfailed')

        if self.modelfield_instance.multi_file_check_function:
            rval = self.modelfield_instance.multi_file_check_function(self.uuid, os.path.split(tmpdir)[0], tmpfiles)
            if not rval['status']:
                raise django.core.exceptions.ValidationError(_('File validation failed'),
                                                             code='filevalidationfailed')

            self.modelfield_instance.multi_check_result = rval

        self.process_post_check(value, self.uuid, tmpfiles)

        if not os.path.isdir(os.path.join(settings.PERSISTENT_DIR, self.file_storage)):
            os.mkdir(os.path.join(settings.PERSISTENT_DIR, self.file_storage))

        if not os.path.isdir(final_storage) and not self.modelfield_instance.allow_multiple_files:
            os.mkdir(final_storage)

        if not self.modelfield_instance.allow_multiple_files:
            shutil.move(src_file, dst_file)
            shutil.rmtree(tmpdir)
            return os.path.join(self.uuid, tmpfiles[0])
        else:
            with open(dst_file, 'wb') as dst_opened:
                with zipfile.ZipFile(dst_opened, 'w', zipfile.ZIP_DEFLATED) as dst_zip:
                    for cur_file in tmpfiles:
                        dst_zip.write(os.path.join(src_file, cur_file), cur_file)
            shutil.rmtree(tmpdir)
            return '%s.zip' % (self.uuid,)

    def process_pre_check(self, value, uuid, file_list):
        pass

    def process_post_check(self, value, uuid, file_list):
        pass


    def _get_tmpdir(self):
        return os.path.join(self.modelfield_instance.upload_view.upload_tmp, self.uuid)

    def prepare_value(self, value):
        return value
