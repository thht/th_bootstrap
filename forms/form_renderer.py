# Copyright (c) 2016-2018, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project, see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

import bootstrap3.renderers

import th_django.forms.widgets


class FormRenderer(bootstrap3.renderers.FormRenderer):
    def _render(self):
        return self.render_fields()


class FieldRenderer(bootstrap3.renderers.FieldRenderer):
    def fix_datetime_select_input(self, html):
        div1 = '<div class="col-xs-4">'
        div2 = '</div>'
        html = html.replace('<select', div1 + '<select')
        html = html.replace('</select>', '</select>' + div2)
        return '<div class="row bootstrap3-multi-input">' + html + '</div>'

    def post_widget_render(self, html):
        html = super().post_widget_render(html)

        if isinstance(self.widget, th_django.forms.widgets.SelectDateTimeWidget):
            html = self.fix_datetime_select_input(html)

        return html
