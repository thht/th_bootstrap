# Copyright (c) 2016-2018, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project, see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

import json
import os.path
import uuid

import django.core.serializers
import django.core.signing
from django.forms.widgets import FileInput
from django.urls import reverse

import th_django.utils.fun2string
import th_django.utils.misc
from th_django.utils.urls import reverse_classname


class AjaxBootstrapFileInput(FileInput):
    input_type = 'file'
    needs_multipart_form = True
    template_name = 'widgets/ajaxbootstrap_fileinput.html'

    def __init__(self, attrs = None):
        super().__init__(attrs)
        self.modelfield_instance = None
        self._salt = th_django.utils.misc.random_string(16)
        self._upload_uuid = django.core.signing.Signer(salt=self._salt).sign(str(uuid.uuid4()))

    @property
    def allow_multiple_files(self):
        return self.modelfield_instance.allow_multiple_files

    def build_attrs(self, base_attrs, extra_attrs=None):
        attrs = super().build_attrs(base_attrs, extra_attrs)
        attrs.update(self._build_data_attrs())
        attrs['multiple'] = self.allow_multiple_files
        attrs['data-mri-valid'] = True
        return attrs

    def get_hidden_inputs(self):
        signer = django.core.signing.Signer(salt=self._salt)

        hidden_inputs = {
            'uuid': {
                'value': self._upload_uuid
            },
            'salt': {
                'value': self._salt
            },
            'allowed_fileextensions': {
                'value': signer.sign(json.dumps(self.modelfield_instance.file_extensions))
            },
        }

        if self.modelfield_instance.single_file_check_function:
            hidden_inputs['single_file_check_function'] = {
                'value': signer.sign(
                    th_django.utils.fun2string.fun2string(self.modelfield_instance.single_file_check_function))
            }

        if self.modelfield_instance.multi_file_check_function:
            hidden_inputs['multi_file_check_function'] = {
                'value': signer.sign(
                    th_django.utils.fun2string.fun2string(self.modelfield_instance.multi_file_check_function)
                )
            }

        return hidden_inputs

    def get_context(self, name, value, attrs):
        self._name = name
        context = super().get_context(name, value, attrs)
        context['hidden_inputs'] = self.get_hidden_inputs()
        context['hidden_inputs'].update({
            'files': {
                'value': ''
            }
        })

        signed_filename = django.core.signing.Signer(salt=self._salt).sign(
            os.path.join(self.modelfield_instance.file_storage, str(value)))

        context['download_button'] = {
            'name': context['widget']['name'] + '_download',
            'show': value is not None and value.file_name != '' and self.modelfield_instance.allow_download,
            'href': reverse(reverse_classname(self.modelfield_instance.download_view),
                            kwargs={'filename': signed_filename, 'salt': self._salt})
        }

        context['current_filename'] = {
            'f_name': os.path.basename(str(value)),
            'show': value is not None and value.file_name != '' and self.modelfield_instance.show_current_file_name,
        }

        if self.modelfield_instance.show_messages:
            context['message_div'] = {
                'name': context['widget']['name'] + '_messages',
            }

        return context

    def value_from_datadict(self, data, files, name):
        this_salt = data[name + '_salt']
        return {
            'data': data[name + '_files'],
            'uuid': django.core.signing.Signer(salt=this_salt).unsign(data[name + '_uuid']),
        }

    def format_value(self, value):
        return value

    def _build_data_attrs(self):
        hidden_inputs_keys = ['%s_%s' % (self._name, cur_key) for cur_key in self.get_hidden_inputs().keys()]

        hidden_input_dict = dict(zip(self.get_hidden_inputs().keys(), hidden_inputs_keys))

        data_attrs = {'uploadUrl': reverse(reverse_classname(self.modelfield_instance.upload_view)),
                      'postUploadUrl': reverse(reverse_classname(self.modelfield_instance.post_upload_view)),
                      'hiddenInput': json.dumps(hidden_input_dict),
                      'autoUpload': str(self.modelfield_instance.auto_upload),
                      'showPreview': str(self.modelfield_instance.show_preview),
                      'fileExtensions': json.dumps(self.modelfield_instance.file_extensions),
                      'allowMultipleFiles': str(self.allow_multiple_files),
                      'showMessages': str(self.modelfield_instance.show_messages),
                      'messages': json.dumps(self.modelfield_instance.messages),
                      'required': json.dumps(self.is_required)
                      }

        return self._prepend_data_attrs(data_attrs)

    def _prepend_data_attrs(self, attrs):
        return {'data-fileupload-' + str(key): (self._prepend_data_attrs(value) if isinstance(value, dict) else value) for key, value in
                attrs.items()}

    @property
    def uuid(self) -> str:
        return self._upload_uuid
